package com.backspace119.watchables;

import java.util.List;

public interface WList<T> extends Watchable<T>,List<T>{
    boolean add(T val);
    boolean removeNoProp(Object val);
    T removeNoProp(int index);
    void clear();
    List<T> get();
}
