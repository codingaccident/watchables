package com.backspace119.watchables;

public interface PassthroughValueWatchable<T> extends ValueWatchable<T> {
    void setWatchable(ValueWatchable<T> watchable);
}
