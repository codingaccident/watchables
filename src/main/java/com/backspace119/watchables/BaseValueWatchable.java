package com.backspace119.watchables;

import java.util.ArrayList;
import java.util.List;

public class BaseValueWatchable<T> implements ValueWatchable<T> {

    public BaseValueWatchable(T val)
    {
        this.val = val;
    }

    protected List<Watcher<T>> watchers = new ArrayList<>();
    protected List<Watcher<T>> singleEventWatchers = new ArrayList<>();
    protected T val;

    @Override
    public synchronized void registerWatcher(Watcher<T> watcher,boolean prop) {
        watchers.add(watcher);
        if(prop)
            watcher.update(this,val);
    }

    @Override
    public synchronized void registerSingleEventWatcher(Watcher<T> watcher,boolean prop) {
        singleEventWatchers.add(watcher);
        if(prop)
            watcher.update(this,val);
    }

    @Override
    public synchronized void removeWatcher(Watcher<T> watcher) {
        watchers.remove(watcher);
        singleEventWatchers.remove(watcher);
    }

    @Override
    public void prop() {
        for (Watcher<T> w : watchers)
            w.update(this, val);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,val);
        singleEventWatchers.clear();
    }


    @Override
    public synchronized void clearWatchers() {
        singleEventWatchers.clear();
        watchers.clear();
    }

    @Override
    public void setNoProp(T val){
        this.val = val;
    }



    @Override
    public void set(T val) {
        this.val = val;
        prop();
    }

    @Override
    public T get() {
        return val;
    }


    @Override
    public String toString()
    {
        return "" + val;
    }
}
