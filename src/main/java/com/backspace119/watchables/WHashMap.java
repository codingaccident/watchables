package com.backspace119.watchables;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WHashMap<K,V> extends HashMap<K,V> implements WMap<K,V>  {
    private List<Watcher<V>> watchers = new ArrayList<>();
    private List<Watcher<V>> singleValueWatchers = new ArrayList<>();
    @Override
    public void registerWatcher(Watcher<V> watcher,boolean prop) {
        watchers.add(watcher);
        if(prop)
            watcher.update(this,null);
    }

    @Override
    public void removeWatcher(Watcher<V> watcher) {
        watchers.remove(watcher);
    }

    @Override
    public void registerSingleEventWatcher(Watcher<V> watcher, boolean propOnRegister) {

    }

    @Override
    public void clearWatchers() {
        watchers.clear();
        singleValueWatchers.clear();
    }

    @Override
    public void prop() {

    }

    @Override
    public V put(K key, V value) {
        V res = super.put(key,value);
        for(Watcher<V> watcher:watchers)
            watcher.update(this,res);
        for(Watcher<V> watcher:singleValueWatchers)
            watcher.update(this,res);
        singleValueWatchers.clear();
        return res;
    }
    @Override
    public V remove(Object o) {
        V res = super.remove(o);
        for(Watcher<V> watcher:watchers)
            watcher.update(this,res);
        for(Watcher<V> watcher:singleValueWatchers)
            watcher.update(this,res);
        singleValueWatchers.clear();
        return res;
    }

    @Override
    public void putAll(Map m) {
        super.putAll(m);
        for(Watcher<V> watcher:watchers)
            watcher.update(this,null);
        for(Watcher<V> watcher:singleValueWatchers)
            watcher.update(this,null);
        singleValueWatchers.clear();
    }
}
