package com.backspace119.watchables;

public class WBoolean extends BaseValueWatchable<Boolean> {

    public WBoolean()
    {
        super(false);
    }

}
