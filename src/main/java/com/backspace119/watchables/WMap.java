package com.backspace119.watchables;

import java.util.Map;

public interface WMap<K,V> extends Watchable<V>, Map<K,V> {

}
