package com.backspace119.watchables;

@FunctionalInterface
public interface Watcher<T> {
    void update(Watchable<T> w, T val);
}
