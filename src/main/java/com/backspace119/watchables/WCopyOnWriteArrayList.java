package com.backspace119.watchables;

import java.util.concurrent.CopyOnWriteArrayList;

public class WCopyOnWriteArrayList<T> extends WArrayList<T> {
    public WCopyOnWriteArrayList()
    {
        super();
        list = new CopyOnWriteArrayList<>();
    }
}
