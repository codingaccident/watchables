package com.backspace119.watchables;

import java.math.BigInteger;

public class WBigInteger extends BaseValueWatchable<BigInteger> {
    public WBigInteger()
    {
        super(BigInteger.ZERO);
    }
}
