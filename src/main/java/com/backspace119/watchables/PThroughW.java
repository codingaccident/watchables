package com.backspace119.watchables;

import java.util.ArrayList;
import java.util.List;

public class PThroughW<T> implements PassthroughValueWatchable<T> {

    private ValueWatchable<T> val;
    private List<Watcher<T>> watchers = new ArrayList<>();
    private List<Watcher<T>> singleEventWatchers = new ArrayList<>();

    public PThroughW()
    {

    }
    public PThroughW(ValueWatchable<T> val)
    {
        this.val = val;
    }

    @Override
    public void registerWatcher(Watcher<T> watcher, boolean propOnRegister) {
            watchers.add(watcher);
            if(propOnRegister)
                watcher.update(val,val.get());
    }

    @Override
    public void removeWatcher(Watcher<T> watcher) {
        watchers.remove(watcher);
    }

    @Override
    public void registerSingleEventWatcher(Watcher<T> watcher, boolean propOnRegister) {
        singleEventWatchers.add(watcher);
        if(propOnRegister)
            watcher.update(val,val.get());
    }

    @Override
    public void clearWatchers() {
        watchers.clear();
    }

    @Override
    public void prop() {
        for(Watcher<T> w:watchers)
            w.update(val,val.get());
        for(Watcher<T> w:singleEventWatchers)
            w.update(val,val.get());
        singleEventWatchers.clear();
    }

    @Override
    public void set(T t) {
        val.set(t);
        prop();
    }

    @Override
    public T get() {
        return val.get();
    }

    @Override
    public void setNoProp(T t) {
        val.setNoProp(t);
    }

    @Override
    public void setWatchable(ValueWatchable<T> watchable) {
        val = watchable;
        val.registerWatcher((w,v) -> prop(),false);
    }
}
