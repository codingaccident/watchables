package com.backspace119.watchables;

public interface ValueWatchable<T> extends Watchable<T>{
    void set(T t);

    T get();
    void setNoProp(T t);

}
