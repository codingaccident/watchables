package com.backspace119.watchables;

import java.util.*;

public class WArrayList<T> implements WList<T> {

    private List<Watcher<T>> watchers = new ArrayList<>();
    private List<Watcher<T>> singleEventWatchers = new ArrayList<>();
    protected List<T> list = new ArrayList<>();
    public WArrayList()
    {

    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return list.iterator();
    }

    @Override
    public Object[] toArray() {
        return list.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return list.toArray(a);
    }

    @Override
    public boolean add(T val) {
        boolean res =list.add(val);
        for(Watcher<T> w:watchers)
            w.update(this,val);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,val);
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public boolean removeNoProp(Object val) {
        return list.remove(val);
    }

    @Override
    public T removeNoProp(int index) {
        return list.remove(index);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return list.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        boolean res = list.addAll(c);
        for(T val:c) {
            for (Watcher<T> w : watchers)
                w.update(this, val);
            for (Watcher<T> w : singleEventWatchers)
                w.update(this, val);
        }
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        boolean res = list.addAll(c);
        for(T val:c) {
            for (Watcher<T> w : watchers)
                w.update(this, val);
            for (Watcher<T> w : singleEventWatchers)
                w.update(this, val);
        }
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean res = list.removeAll(c);
        for(Object val:c) {
            for (Watcher<T> w : watchers)
                w.update(this, (T) val);
            for (Watcher<T> w : singleEventWatchers)
                w.update(this, (T) val);
        }
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean res = list.retainAll(c);
        for(Object val:c) {
            for (Watcher<T> w : watchers)
                w.update(this, (T) val);
            for (Watcher<T> w : singleEventWatchers)
                w.update(this, (T) val);
        }
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public boolean remove(Object val) {
        boolean res = list.remove(val);
        //TODO fix this
        if(val != null)
        for(Watcher<T> w:watchers)
            w.update(this,(T)val);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,(T)val);
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public void clear() {
        //TODO clarify this, find a better way of doing this
        list.clear();
        for(Watcher<T> w:watchers)
            w.update(this,null);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,null);
        singleEventWatchers.clear();
    }

    @Override
    public T get(int index) {
        return list.get(index);
    }

    @Override
    public T set(int index, T element) {
        T res = list.set(index,element);
        for(Watcher<T> w:watchers)
            w.update(this,element);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,element);
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public void add(int index, T element) {
         list.add(index,element);
        for(Watcher<T> w:watchers)
            w.update(this,element);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,element);
        singleEventWatchers.clear();

    }

    @Override
    public T remove(int index) {
        T res = list.remove(index);
        for(Watcher<T> w:watchers)
            w.update(this,res);
        for(Watcher<T> w:singleEventWatchers)
            w.update(this,res);
        singleEventWatchers.clear();
        return res;
    }

    @Override
    public int indexOf(Object o) {
        return list.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return list.lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return list.listIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return list.listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return list.subList(fromIndex,toIndex);
    }

    @Override
    public List<T> get() {
        return list;
    }


    @Override
    public void registerWatcher(Watcher<T> watcher,boolean prop) {
        watchers.add(watcher);
        if(prop)
        for(T v:list)
            watcher.update(this,v);
    }

    @Override
    public void removeWatcher(Watcher<T> watcher) {
        watchers.remove(watcher);
        singleEventWatchers.remove(watcher);
    }

    @Override
    public void registerSingleEventWatcher(Watcher<T> watcher, boolean propOnRegister) {
        singleEventWatchers.add(watcher);
        if(propOnRegister)
            for(T v:list)
                watcher.update(this,v);
    }

    @Override
    public void clearWatchers() {
        watchers.clear();
        singleEventWatchers.clear();
    }

    @Override
    public void prop() {
        //can't work with this type of watchable
    }
}
